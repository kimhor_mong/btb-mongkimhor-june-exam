import api from '../utils/apis';

export const fetchTutorials = async () => {
	let response = await api.get('/tutorials');
	console.log(response);
	return response.data.data;
};

export const postTutorial = async (tutorial) => {
	let response = await api.post('/tutorials', tutorial);
	return response.data.data;
};

export const getTutorialById = async (id) => {
	let response = await api.put('/tutorials/' + id);
	return response.data.data;
};
