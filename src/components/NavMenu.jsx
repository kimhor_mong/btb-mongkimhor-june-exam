import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';

function NavMenu() {
	return (
		<Navbar bg='light' expand='lg'>
			<Container>
				<Navbar.Brand as={NavLink} to='/'>
					React Router
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className='me-auto'>
						<Nav.Link as={Link} to='/'>
							Console
						</Nav.Link>
						<Nav.Link as={NavLink} to='/tutorial'>
							Redux
						</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default NavMenu;
