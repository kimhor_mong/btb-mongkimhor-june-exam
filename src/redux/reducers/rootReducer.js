import { combineReducers } from 'redux';
import tutorialReducer from './tutorialReducer';

const rootReducer = combineReducers({ tutorials: tutorialReducer });

export default rootReducer;
