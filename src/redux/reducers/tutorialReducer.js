import { tutorialActionType } from '../actions/tutorialActionType';

const initialState = {
	tutorials: [],
};

const tutorialReducer = (state = initialState, action) => {
	switch (action.type) {
		case tutorialActionType.FETCH_TUTORIALS:
			return { ...state, tutorials: [...action.payload] };
		default:
			return state;
	}
};

export default tutorialReducer;
