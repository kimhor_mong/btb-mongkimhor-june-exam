import { fetchTutorials } from '../../services/tutorialService';
import { tutorialActionType } from './tutorialActionType';

export const onFetchTutorials = () => async (dispatch) => {
	let tutorials = await fetchTutorials();
	dispatch({
		type: tutorialActionType.FETCH_TUTORIALS,
		payload: tutorials,
	});
};
