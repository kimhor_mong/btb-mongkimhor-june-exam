import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import NavMenu from './components/NavMenu';
import Tutorials from './pages/Tutorials';
import Console from './pages/Console';
function App() {
	return (
		<Router>
			<NavMenu />
			<Container>
				<Switch>
					{/* <Route exact path='/' component={Home} />
					<Route path='/detail/:id' component={ArticleDetail} />
					<Route path='/video' component={Video} />
					<Route path='/account' component={Account} />
					<ProtectedRoute path='/welcome'>
						<Welcome />
					</ProtectedRoute> */}
					<Route exact path='/' component={Console} />
					<Route path='/tutorail' component={Tutorials} />
				</Switch>
			</Container>
		</Router>
	);
}

export default App;
