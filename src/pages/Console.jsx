import React from 'react';
import { fetchTutorials, postTutorial } from '../services/tutorialService';
import { Button } from 'react-bootstrap';

function Home() {
	const getTutotials = () => {
		fetchTutorials()
			.then((response) => console.log(response))
			.catch((error) => console.log(error.message));
	};

	const addTutotial = () => {
		const toturial = {
			title: 'string',
			description: 'string',
		};
		postTutorial(toturial)
			.then((response) => console.log(response))
			.catch((error) => console.log(error.message));
	};

	return (
		<div>
			<h1>In console</h1>
			<Button onClick={getTutotials}>Get Tutorial</Button> <Button onClick={addTutotial}>Add Tutorial</Button>
		</div>
	);
}

export default Home;
