import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchTutorials } from '../redux/actions/tutorialAction';

function Tutorials() {
	const tutorials = useSelector((state) => state.tutorials.tutorials);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchTutorials());
	}, []);

	return (
		<Container>
			<h1 className='my-3'>Tutorials</h1>
		</Container>
	);
}

export default Tutorials;
